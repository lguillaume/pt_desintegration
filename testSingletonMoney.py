import unittest
from moduleResources import *

class TestSingletonMoney(unittest.TestCase):

    def test_money_singleton(self):
        money = Money('name', 5)
        money2 = Money('otherName', 4)

        self.assertEqual(money.money, money2.money)
        self.assertEqual(money.money.number, 5)
      

if __name__ == '__main__':
    unittest.main()
