from caylusMC import Game, GameElement
import unittest
import collections


class TestRemoveToken(unittest.TestCase):

    def setUp(self):
        self.game = GameElement().game
        self.game.setup()
        

    def test_remove_token_castl(self):
        self.game.current_n_castle_tokens = 8
        removedToken = self.game.remove_tokens_castle(3)
        self.assertEqual(self.game.current_n_castle_tokens, 5)
        self.assertEqual(removedToken, 3)

    def test_remove_token_castle_more_than_max(self):
        self.game.current_n_castle_tokens = 3
        removedToken = self.game.remove_tokens_castle(5)
        self.assertEqual(self.game.current_n_castle_tokens, 0)
        self.assertEqual(removedToken, 3)
        
    
if __name__ == '__main__':
    unittest.main()