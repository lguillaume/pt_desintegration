import itertools
from modulePlayer import Player, ColorPlayer
from moduleBuildings import Building
from moduleResources import Resource, MoneyResource
from moduleEnums import Location
from moduleGeneral import indent, TXT_SEPARATOR



class HumanPlayer(Player):
    """Human player."""

    def __init__(self, color_player: ColorPlayer):
        """Initialization of an AI player."""
        Player.__init__(self, color_player)

    def is_human(self) -> bool:
        """Indicates that it is an human player."""
        return True

    def name(self) -> str:
        """Get the name of the human player."""
        return '"' + self.color_player.name + Player.txt_separator_name + 'you!"'

    def print_buildings_by_location(self, n_indent: int) -> None:
        """Display the player buildings of the player that is the location of the buildings in the deck: in the pile, the hand and the discard (but neither along the road nor replaced) ."""
        print(indent(n_indent) + self.name() + ', location of your buildings: ')
        for location in Location:
            player_building_names_by_location = sorted([player_building.name
                                                        for player_building in
                                                        self.get_player_buildings_by_location(location)]
                                                       )  # type: List[PlayerBuilding]
            if player_building_names_by_location:
                print(indent(n_indent + 1) + location.name.lower() + ': ' +
                      TXT_SEPARATOR.join(player_building_names_by_location) + '.')

    def choose_discard_hand_for_new(self) -> bool:
        self.print_buildings_by_location(0)
        response = input('Do you want to discard all the cards in your hand and to take new cards? [Y/N] ')  # type: str
        return self.check_response_yes_no(response, 1)

    def choose_action(self, possible_actions):
        self.print_buildings_by_location(3)
        n_possible_actions = len(possible_actions)  # type: int # >= 1 because it must contain the passing action.
        if n_possible_actions == 1:
            print(indent(3) + 'You don\'t have any choice and you have to do the action: ' + possible_actions[0][1])
            return possible_actions[0]
        else:
            print(indent(3) + 'Here are all the possible action(s):')
            for i_possible_actions, possible_action in enumerate(possible_actions):
                print(indent(4) + str(i_possible_actions) + ': ' + possible_action[1])
            response = input(indent(3) +
                             'Which action do you choose? [0..' + str(n_possible_actions - 1) + '] ')  # type: str
            return possible_actions[self.check_response_in_interval(response, 0, n_possible_actions - 1, 4)]

    def choose_n_provost_movement(self, n_min_provost_movements_player: int,
                                  n_max_provost_movements_player: int) -> int:
        response = input(indent(3) + 'How long do you want to move the Provost? [' +
                         str(n_min_provost_movements_player) + '..' + str(n_max_provost_movements_player) +
                         '] ')  # type: str
        return self.check_response_in_interval(response, n_min_provost_movements_player, n_max_provost_movements_player,
                                               4)

    def choose_buy_resource(self, money_resource_cost: MoneyResource, qty_cost: int, resource_gain_choices,
                            qty_gain: int) -> Resource:
        possibilities = list(Building.ABBREV_NO_USE_EFFECT)  # type: List[str[1]]
        abbrev_resource_name_resource = {}  # type: Dict[str[1], Resource] # E.g. {'F': food, ...}.
        for resource in resource_gain_choices:
            abbrev_resource_name = resource.get_name_abbreviation()
            abbrev_resource_name_resource[abbrev_resource_name] = resource
            possibilities.append(abbrev_resource_name)
        print(indent(4) + self.txt_name_money_resources_workers_PPs_deck(True, True, False, False, False) + '.')
        response = input(indent(4) + 'Do you want to consume ' + str(qty_cost) + ' ' + money_resource_cost.name +
                         '(s) to obtain ' + str(qty_gain) + ' resource ' + Building.TXT_NO_USE_EFFECT +
                         '? [' + '/'.join(possibilities) + '] ')  # type: str
        response = self.check_response_in_possibilities(response, possibilities, 5)
        return None if response == Building.ABBREV_NO_USE_EFFECT else abbrev_resource_name_resource[response]

    def choose_buy_resource_multi(self, costs, resource_gain_choices, qty_gain: int):  # -> List[Resource]
        # Remark: we only consider the case that costs is a List[Tuple[Money,int]] without resource and of length 2 or more.
        possibilities = list(Building.ABBREV_NO_USE_EFFECT)  # type: List[str[1..qty_gain]]
        abbrev_resource_name_resource = {resource.get_name_abbreviation(): resource
                                         for resource in resource_gain_choices
                                         }  # type: Dict[str[1], Resource] # E.g. {'F': food, ...}.
        possibilities.extend([''.join(
            resource_gain.get_name_abbreviation() for resource_gain in list(resource_gain_choice))
            for n_parts in range(1, qty_gain + 1)
            for resource_gain_choice in itertools.combinations_with_replacement(resource_gain_choices, n_parts)])
        print(indent(4) + self.txt_name_money_resources_workers_PPs_deck(True, True, False, False, False) + '.')
        response = input(indent(4) + 'Do you want to consume either ' +
                         ' or '.join(str(qty_cost) + ' ' + money_resource_cost.name +
                                     '(s) for ' + str(1 + i_costs) + ' resource(s)'
                                     for i_costs, (money_resource_cost, qty_cost) in enumerate(costs)) +
                         ' ' + Building.TXT_NO_USE_EFFECT + '? [' + '/'.join(possibilities) + '] ')  # type: str
        response = self.check_response_in_possibilities(response, possibilities, 5)
        choose_buy_resource_multi = list()
        if response != Building.ABBREV_NO_USE_EFFECT:
            for abbrev_resource_name in response:
                choose_buy_resource_multi.append(abbrev_resource_name_resource[abbrev_resource_name])
        return choose_buy_resource_multi

    def choose_buy_castle_multi(self, costs, castle_gain_choices):  # -> List[Castle]
        n_min_castle_gain = 0  # type: int
        n_max_castle_gain = len(castle_gain_choices)  # type: int
        response = input(indent(4) + 'How many tokens of parts of the castle among [' +
                         ', '.join(castle.name for castle in castle_gain_choices) + '] do you want to buy with: ' +
                         ' or '.join(str(qty_cost) + ' ' + money_resource_cost.name + '(s)'
                                     for (money_resource_cost, qty_cost) in costs) +
                         '? [' + str(n_min_castle_gain) + '..' + str(n_max_castle_gain) + '] ')  # type: str
        return castle_gain_choices[:self.check_response_in_interval(response, n_min_castle_gain, n_max_castle_gain, 5)]

    def choose_exchange_resource(self, can_no_use_effect: bool, qty_cost: int, resource_cost_choices,
                                 money_resource_gain: MoneyResource, qty_gain: int) -> Resource:
        abbrev_resource_name_resources = Resource.get_name_abbreviation_resources(resource_cost_choices)
        possibilities = list()  # type: List[str[1]]
        if can_no_use_effect:
            possibilities.append(Building.ABBREV_NO_USE_EFFECT)
        possibilities += list(abbrev_resource_name_resources.keys())
        print(indent(4) + self.txt_name_money_resources_workers_PPs_deck(True, True, False, False, False) + '.')
        response = input(indent(4) + 'Do you want to exchange '
                         + str(qty_cost) + ' of some resource with ' + str(qty_gain) + ' ' + money_resource_gain.name +
                         (' or not use the effect ' + Building.TXT_NO_USE_EFFECT if can_no_use_effect else '') +
                         '? [' + '/'.join(possibilities) + '] ')  # type: str
        response = self.check_response_in_possibilities(response, possibilities, 5)
        return None if response == Building.ABBREV_NO_USE_EFFECT else abbrev_resource_name_resources[response]

    def choose_construct_residence(self, resource_costs, i_road_buildings_on_road):  # -> Optional[Tuple[int, Building]]
        n_min_i_building = 0  # type: int
        n_max_i_building = len(i_road_buildings_on_road)  # type: int
        response = input(indent(4) + 'Which of your ' + str(n_max_i_building) + ' building(s) [' +
                         ', '.join(building_on_road.name for (i_road, building_on_road) in i_road_buildings_on_road) +
                         '] do you want to choose (or ' + str(n_min_i_building) +
                         ' if you don\'t want to use the effect)? [' + str(n_min_i_building) + '..' +
                         str(n_max_i_building) + '] ')  # type: str
        response = self.check_response_in_interval(response, n_min_i_building, n_max_i_building, 5)  # type: int
        return None if response == n_min_i_building else i_road_buildings_on_road[response - 1]

    def choose_n_batches_to_castle(self, n_max_batches_to_castle: int) -> int:
        response = input(indent(3) + 'How many batches do you want to offer to the castle? [0..' +
                         str(n_max_batches_to_castle) + '] ')  # type: str
        return self.check_response_in_interval(response, 0, n_max_batches_to_castle, 4)

    def check_response_yes_no(self, response: str, n_indent: int) -> bool:
        """Check that a (string) response is (a boolean) yes or no."""
        return self.check_response_in_possibilities(response, ['Y', 'N'], n_indent) == 'Y'

    def check_response_in_possibilities(self, response: str, possibilities, n_indent: int) -> str:
        """Check that a (string) response is in a list of (str[1..2]) possibilities."""
        while len(response) == 0 or response.upper() not in possibilities:
            response = input(indent(n_indent) + 'Please: ')
        return response.upper()

    def check_response_in_interval(self, response: str, n_min: int, n_max: int, n_indent: int) -> int:
        """Check that a (string) response is in an interval (of numeric values)."""
        is_response_ok = False  # type: bool
        while not is_response_ok:
            try:
                response_ok = int(response)  # type: int
                is_response_ok = n_min <= response_ok <= n_max  # Is in the interval?
            except ValueError:
                is_response_ok = False  # Not a numeric.
            if not is_response_ok:
                response = input(indent(n_indent) + 'Please: ')
        return response_ok
