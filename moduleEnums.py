from enum import Enum, unique

@unique
class Action(Enum):
    """Enumeration of all the possible actions of the phase Actions."""
    PICK_CARD = ('Pick a card')
    REPLACE_CARDS_IN_HAND = ('Replace all the cards in your hand')
    PLACE_WORKER_ON_BUILDING = ('Place a worker on a building')
    CONSTRUCT_BUILDING_FROM_HAND = ('Construct a building from your hand')
    CONSTRUCT_PRESTIGE_BUILDING_BEGINNER = ('Construct a prestige building (Beginner version)')
    CONSTRUCT_PRESTIGE_BUILDING_STANDARD = ('Construct a prestige building (Standard version)')
    PASSING = ('Passing')

    def __init__(self, txt: str):
        """Initialization of an action."""
        self.txt = txt  # Type: str

@unique
class BuildingType(Enum):
    """Enumeration of all the types of buildings."""
    """ We choose to code the building type with an enumeration instead of using .__class__ or type() into Building classes. """
    PRESTIGE = 0
    NEUTRAL = 1
    BACKGROUND = 2
    PLAYER = 3


    
@unique
class Location(Enum):
    """Enumeration of all the possible locations of the player buildings."""
    HAND = 0
    PILE = 1
    DISCARD = 2
    ROAD = 3
    REPLACED = 4