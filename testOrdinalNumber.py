import unittest
from moduleGeneral import ordinal_number

class TestOrdinalNumber(unittest.TestCase):

    def test_ordinal_number_exception_negative(self):
        with self.assertRaises(Exception):
            ordinal_number(-1)


    def test_ordinal_number_exception_zero(self):
        with self.assertRaises(Exception):
            ordinal_number(0)
    
    def test_ordinal_number_1_equals_st(self):
        self.assertEquals(ordinal_number(1), '1st')

    def test_ordinal_number_2_equals_nd(self):
        self.assertEquals(ordinal_number(2), '2nd')

    def test_ordinal_number_3_equals_rd(self):
        self.assertEquals(ordinal_number(3), '3rd')

    def test_ordinal_number_5_equals_th(self):
        self.assertEquals(ordinal_number(5), '5th')


if __name__ == '__main__':
    unittest.main()