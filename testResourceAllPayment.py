import unittest
import collections
from modulePlayer import Player, ColorPlayer
from moduleResources import *

class TestResourceAllPayment(unittest.TestCase):

    def setUp(self):
        #3F,2W,3S,3G
        player = Player(ColorPlayer('blue'))
        player.current_money_resources = { 
            self.food: 3,
            self.wood: 2,
            self.stone: 3,
            self.gold: 3
        }# type: Dict[Ressource, int]
        self.player = player

    def test_resource_payment_first_test(self):
        #Dictionnary for the costs
        #-1F,-3W,(S),-1G
        dictionnaryCost = {
            self.food: 1,
            self.wood: 3,
            self.stone: 0,
            self.gold: 1
        } # type: List[Dict[Ressource, int]]


        #Dictionnary for the payment
        #-1F,-2W,-0S,-1G-1G
        dictionnaryResourcePayment =  {
            self.food: 1,
            self.wood: 2,
            self.stone: 0,
            self.gold: 2
        }# type: List[Dict[Ressource, int]]

        resourcePayment = self.player.resource_all_payments(dictionnaryCost)
        self.assertDictEqual(resourcePayment, dictionnaryResourcePayment)


if __name__ == '__main__':
    unittest.main()